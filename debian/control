Source: pushover
Section: games
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders: Gürkan Myczko <gurkan@phys.ethz.ch>
Build-Depends: debhelper (>= 11), liblua5.3-dev, libsdl-mixer1.2-dev, libsdl-ttf2.0-dev, libfribidi-dev, povray, libboost-dev, libsdl-image1.2-dev, libboost-filesystem-dev, libboost-system-dev
Standards-Version: 4.4.1
Vcs-Browser: https://salsa.debian.org/games-team/pushover
Vcs-Git: https://salsa.debian.org/games-team/pushover.git
Homepage: https://pushover.github.io/

Package: pushover
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, fonts-freefont-ttf
Recommends: pushover-data
Description: Fun puzzle game with dominos
 This is a fun puzzle game originally published by Ocean in 1992. In this
 game you control an ant that can walk along platforms that are connected
 with ladders. On those platforms are dominos that need to fall according
 to some rules.
 .
  - All dominos must fall and none must crash into another
  - One special domino must fall as last domino and that domino triggers
    the exit door to open when you enter the exit door the level has been
    completed
  - You may rearrange as many dominos as you want, except for the trigger.
    You may not place dominos in front of the doors, except for the vanishing
    domino.
  - You may push once to start a chain reaction with the dominos leading to
    the fall of all of them
  - All this has to be done within a time limit (which is normally generous)
  - There are 10 different dominos that behave differently when pushed, some
    fall, some not, some wait a bit before they fall, some raise, some toppler
    until they meet an obstacle
  - There is a help in the game and introductory levels that show how all the
    dominos work

Package: pushover-data
Architecture: all
Depends: ${misc:Depends}
Breaks: pushover (<< 0.0.5+git20180909)
Replaces: pushover (<< 0.0.5+git20180909)
Description: Fun puzzle game with dominos, music files
 This is a fun puzzle game originally published by Ocean in 1992. In this
 game you control an ant that can walk along platforms that are connected
 with ladders. On those platforms are dominos that need to fall according
 to some rules.
 .
 This package contains the optional background music.
